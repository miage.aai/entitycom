package fr.miage.agentfournisseur1.entitycom;

public enum Categorie
{
    LEGUMES,
    PRODUITS_LAITIERS,
    BOISSONS,
    COSMETIQUES,
    PRODUITS_ENTRETIEN,
    PRODUITS_HIGH_TECH;
}
