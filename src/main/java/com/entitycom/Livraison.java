package fr.miage.agentfournisseur1.entitycom;

import jade.core.AID;

import java.io.Serializable;

public class Livraison implements Serializable {
    private Commande commande;
    private AID destinataire;

    public Livraison(Commande commande, AID destinataire) {
        this.commande = commande;
        this.destinataire = destinataire;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public AID getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(AID destinataire) {
        this.destinataire = destinataire;
    }
}
