package fr.miage.agentfournisseur1.entitycom;

import java.io.Serializable;

public class DetailCommande implements Serializable
{
    private String id;
    private int quantite;
    private double reduction;
    private Produit produit;


    public DetailCommande(String id, int quantite, double reduction, Produit produit) {
        super();
        this.id = id;
        this.quantite = quantite;
        this.reduction = reduction;
        this.produit = produit;
    }

    public Produit getProduit() {
        return produit;
    }
    public void setProduit(Produit produit) {
        this.produit = produit;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public int getQuantite() {
        return quantite;
    }
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    public double getReduction() {
        return reduction;
    }
    public void setReduction(double reduction) {
        this.reduction = reduction;
    }

    public DetailCommande clone() {
        return new DetailCommande(this.getId(), this.getQuantite(), this.getReduction(), this.getProduit());
    }
}