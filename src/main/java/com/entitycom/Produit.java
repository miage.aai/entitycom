package fr.miage.agentfournisseur1.entitycom;

import java.io.Serializable;
import java.sql.Date;

public class Produit implements Serializable{
    private String ID;
    private String nom;
    private Categorie categorie;
    private String marque;
    private int taille;
    private double prix;
    private Date dateInvention;

    public Produit() {}

    public Produit(String iD, String nom, Categorie categorie, String marque, int taille, double prix, Date dateInvention)
    {
        super();
        ID = iD;
        this.nom = nom;
        this.marque = marque;
        this.taille = taille;
        this.prix = prix;
        this.categorie = categorie;
        this.dateInvention = dateInvention;
    }

    public String getID() {
        return ID;
    }
    public void setID(String iD) {
        ID = iD;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getMarque() {
        return marque;
    }
    public void setMarque(String marque) {
        this.marque = marque;
    }
    public int getTaille() {
        return taille;
    }
    public void setTaille(int taille) {
        this.taille = taille;
    }
    public Categorie getCategorie() {
        return categorie;
    }
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
    public double getPrix() {
        return prix;
    }
    public void setPrix(double prix) {
        this.prix = prix;
    }
    public Date getDateInvention() {
        return dateInvention;
    }
    public void setDateInvention(Date dateInvention) {
        this.dateInvention = dateInvention;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "ID='" + ID + '\'' +
                ", nom='" + nom + '\'' +
                ", categorie=" + categorie +
                ", marque='" + marque + '\'' +
                ", taille=" + taille +
                ", prix=" + prix +
                ", dateInvention=" + dateInvention +
                '}';
    }
}
