package fr.miage.agentfournisseur1.entitycom;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Commande implements Serializable{
    private String id;
    private LocalDate date;

    private List<DetailCommande> commande;

    public Commande(String id, LocalDate date, List<DetailCommande> commande) {
        super();
        this.id = id;
        this.date = date;
        this.commande = commande;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public List<DetailCommande> getCommande() {
        return commande;
    }
    public void setCommande(List<DetailCommande> commande) {
        this.commande = commande;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", commande=" + commande +
                '}';
    }
}
